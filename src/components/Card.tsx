import React from 'react'

import '../css/cards.scss'
type cardProps = {
    rank: string;
    suit: string;
}

export  const Card = function (props: cardProps) {
    const displayClass: string = props.suit + "-" + props.rank+" mg-card"
    return (
        <div>
        <div className={displayClass}>&nbsp;</div>
        </div>
            )
}